package edu.alenasoft.sesion9;

/**
 * Created by Luis Roberto Perez on 7/11/2016.
 */
public interface Food {

    String describe();
    int getPrice();
}
