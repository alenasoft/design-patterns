package edu.alenasoft.sesion9;

/**
 * Created by Luis Roberto Perez on 7/11/2016.
 */
public class Pizza implements Food {

    public String describe() {
        return "Classic Pizza";
    }

    public int getPrice() {
        return 48;
    }

}
