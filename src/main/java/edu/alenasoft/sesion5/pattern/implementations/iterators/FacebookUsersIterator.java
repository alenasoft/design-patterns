package edu.alenasoft.sesion5.pattern.implementations.iterators;

import edu.alenasoft.sesion5.pattern.behaviors.Iterator;
import edu.alenasoft.sesion5.pattern.implementations.aggregates.Facebook;

/**
 * Created by Luis Roberto Perez on 19/10/2016.
 */
public class FacebookUsersIterator implements Iterator<String> {

    private int position;
    private Facebook facebook;

    public FacebookUsersIterator(Facebook facebook) {
        this.facebook = facebook;
        this.position = 0;
    }

    public String current() {
        return (String) this.facebook.getUsers().get(this.position);
    }

    public String next() {
        return (String) this.facebook.getUsers().get(this.position++);
    }

    public boolean isDone() {
        return this.position >= this.facebook.getUsers().size();
    }
}
