package edu.alenasoft.sesion5.pattern.implementations.iterators;

import edu.alenasoft.sesion5.pattern.behaviors.Iterator;
import edu.alenasoft.sesion5.pattern.implementations.aggregates.Google;

/**
 * Created by Luis Roberto Perez on 19/10/2016.
 */
public class GoogleUsersIterator implements Iterator<String> {

    private Google google;
    private int position;

    public GoogleUsersIterator(Google google) {
        this.google = google;
        this.position = 0;
    }

    public String current() {
        return this.google.getGooglers()[this.position];
    }

    public String next() {
        return this.google.getGooglers()[this.position++];
    }

    public boolean isDone() {
        return this.position >= this.google.getGooglers().length;
    }

}
