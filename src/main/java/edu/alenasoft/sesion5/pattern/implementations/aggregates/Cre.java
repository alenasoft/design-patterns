package edu.alenasoft.sesion5.pattern.implementations.aggregates;

import edu.alenasoft.sesion5.pattern.implementations.iterators.CreIterator;
import edu.alenasoft.sesion5.pattern.behaviors.Aggregate;
import edu.alenasoft.sesion5.pattern.behaviors.Iterator;

/**
 * Created by Luis Roberto Perez on 19/10/2016.
 */
public class Cre implements Aggregate {
    private int[] ids;

    public Cre() {
        this.ids = new int[3];
        this.ids[0] = 15;
        this.ids[1] = 25;
        this.ids[2] = 3;
    }

    public Iterator createIterator() {
        return new CreIterator(this);
    }

    public int[] getIds() {
        return this.ids;
    }
}
