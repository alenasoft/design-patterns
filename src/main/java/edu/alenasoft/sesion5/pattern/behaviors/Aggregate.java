package edu.alenasoft.sesion5.pattern.behaviors;

/**
 * Created by Luis Roberto Perez on 19/10/2016.
 */
public interface Aggregate {

    Iterator createIterator();
}
