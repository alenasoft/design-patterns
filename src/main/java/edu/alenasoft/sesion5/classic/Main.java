package edu.alenasoft.sesion5.classic;

import java.util.ArrayList;

/**
 * Created by Luis Roberto Perez on 19/10/2016.
 */
public class Main {

    public static void main(String[] args) {

        Facebook facebook = new Facebook();

        //Ver lista de usuarios
        ArrayList<String> facebookUsers = facebook.getUsers();
        for ( int i = 0; i < facebookUsers.size(); i++ ) {
            System.out.println(facebookUsers.get(i));
        }

        // Conocemos a la nueva empresa Google
        Google google = new Google();
        String[] googleUsers = google.getGooglers();
        for (int i = 0; i < googleUsers.length; i++) {
            System.out.println(googleUsers[i]);
        }
    }
}
