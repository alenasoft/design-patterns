package edu.alenasoft.sesion3;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 12/10/2016.
 */
public class Sorter {

    public void sortByName(List countries) {
        Collections.sort(countries);
    }

    public void sortByPeople(List countries) {
        Collections.sort(countries, new ComparatorByPeople());
    }
}
