package edu.alenasoft.sesion7;

import java.util.Random;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public class SoccerGame {
    public static void main(String[] args) {
        // if %2==0 under attack
        Random random = new Random();

        Team oriente = new Team(2500);

        for(int i=0; i<100; i++) {
            int specialCase = 10;
            if(random.nextInt()%2 == 0) {
                oriente.setStrategy(new AttackStrategy());
            } else {
                oriente.setStrategy(new DefenseStrategy());
            }

            if(random.nextInt() > specialCase) {
                oriente.setStrategy(new Strategy() {
                    public void doWork() {
                        System.out.println("Punch the refery");
                    }
                });
            }

            oriente.play();
        }
    }
}
