package edu.alenasoft.sesion10;

/**
 * Created by Luis Roberto Perez on 7/11/2016.
 */
public class SimpleSingletonWithAccess {

    private static final SimpleSingletonWithAccess simpleSingleton = new SimpleSingletonWithAccess();
    private SimpleSingletonWithAccess(){}
    public static SimpleSingletonWithAccess getInstance() {
        return simpleSingleton;
    }
}
