package edu.alenasoft.sesion10;

/**
 * Created by Luis Roberto Perez on 7/11/2016.
 */
public class Main2 {

    public static void main(String[] args) {
        SimpleSingleton ss1 = SimpleSingleton.simpleSingleton;
        System.out.println(ss1);

        SimpleSingleton ss2 = SimpleSingleton.simpleSingleton;
        System.out.println(ss2);
    }
}
