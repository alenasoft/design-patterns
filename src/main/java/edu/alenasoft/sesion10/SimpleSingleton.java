package edu.alenasoft.sesion10;

import java.util.Random;

/**
 * Created by Luis Roberto Perez on 7/11/2016.
 */
public class SimpleSingleton {

    public final int data;
    public static final SimpleSingleton simpleSingleton = new SimpleSingleton();
    private SimpleSingleton(){
        data = new Random().nextInt();
        System.out.println("MyData: " + data);
    }
}
