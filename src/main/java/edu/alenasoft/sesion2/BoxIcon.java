package edu.alenasoft.sesion2;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Luis Roberto Perez on 10/10/2016.
 */
public class BoxIcon implements Icon {

    private int size;
    private Color color;

    public BoxIcon(int size, Color color) {
        this.size = size;
        this.color = color;
    }

    public void paintIcon(Component c, Graphics pen, int x, int y) {
        pen.setColor(this.color);
        pen.fillRect(0,0,this.size, this.size);
    }

    public int getIconWidth() {
        return this.size;
    }

    public int getIconHeight() {
        return this.size;
    }
}
