package edu.alenasoft.sesion8;

/**
 * Created by Luis Roberto Perez on 26/10/2016.
 */
public class Main {
    public static void main(String[] args) {
        Folder root = new Folder("root");
        Folder opt = new Folder("opt");
        Folder usr = new Folder("usr");

        root.addFile(opt);
        root.addFile(usr);

        Folder userFolder = new Folder("roberto");
        usr.addFile(userFolder);

        File curriculo = new WordFile("CV_2016");
        File rebootScript = new BashFile("reboot");

        userFolder.addFile(curriculo);
        userFolder.addFile(rebootScript);

        curriculo.printInfo();
        opt.printInfo();
        usr.printInfo();

        System.out.println("========================");
        root.printInfo();
    }
}
