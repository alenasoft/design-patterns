package edu.alenasoft.evaluation1;

/**
 * Created by Luis Roberto Perez on 31/10/2016.
 */
public class DecreaseQualityUpdater implements Updater {
    public void update(Item item) {
        int delta = (item.getSellIn() > 0) ? 1 : 2;
        item.setQuality(item.getQuality() - delta);
    }
}
