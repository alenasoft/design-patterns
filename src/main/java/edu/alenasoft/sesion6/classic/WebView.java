package edu.alenasoft.sesion6.classic;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public class WebView {

    private Model model;
    private int actualAverage;
    private int oldAverage;

    public WebView(Model model) {
        this.model = model;
        this.actualAverage = model.getAverage();
        this.oldAverage = model.getAverage();
    }

    public void notifyChanges() {
        System.out.println("Average Ages changed");
    }

    private void updateInfo() {
        Timer timer = new Timer(2000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(model.getAverage() != oldAverage) {
                    notifyChanges();
                    oldAverage = model.getAverage();
                }
            }
        });
    }
}
