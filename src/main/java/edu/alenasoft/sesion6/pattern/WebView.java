package edu.alenasoft.sesion6.pattern;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public class WebView implements Observer {
    public void doAction(int age) {
        System.out.println("Via WEB: Average Ages changed " + age);
    }
}
