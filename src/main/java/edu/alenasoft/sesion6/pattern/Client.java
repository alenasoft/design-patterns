package edu.alenasoft.sesion6.pattern;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public class Client {

    public static void main(String[] args) {
        Model model = new Model();

        model.attach(new WebView());
        model.attach(new MobileView());

        Observer webObserver = new WebView();
        model.attach(webObserver);

        model.setAverageAge(500);

        model.detach(webObserver);
        model.setAverageAge(1111);
    }
}
